class CreateCustomers < ActiveRecord::Migration[6.0]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :city
      t.string :country
      t.numeric :height
      t.numeric :weight
      t.string :blood_type
      t.string :marriage_status
      t.string :gender

      t.timestamps
    end
  end
end
