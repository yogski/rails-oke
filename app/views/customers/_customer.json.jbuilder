json.extract! customer, :id, :name, :city, :country, :height, :weight, :blood_type, :marriage_status, :gender, :created_at, :updated_at
json.url customer_url(customer, format: :json)
